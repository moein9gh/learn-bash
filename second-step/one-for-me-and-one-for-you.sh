#!/bin/bash

Name=$1

if [ -z "$Name" ]
then
    echo "One for you, one for me."
else
    echo One for $Name, one for me.
fi
