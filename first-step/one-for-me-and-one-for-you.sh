#!/bin/bash

read Name

if [ -z "$Name" ]
then
    echo "One for you, one for me."
else
    echo One for $Name, one for me.
fi
